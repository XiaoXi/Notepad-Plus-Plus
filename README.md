# Notepad++

⭐ 一个免费且开源的代码编辑器 ⭐

## ✅ 关于我为什么要创建这个分支

有使用 Notepad++ 的朋友都应该有注意到，在 Notepad++ 的下载页面上，经常出现各种各样的由作者 Don Ho 留下的包括但不限于「港台分裂主义」、「美帝国主义」等反华、反智言论，并试图道德绑架所有下载使用该「开源软件」的用户承认该言论。

我们毋须承认，开源项目并非一人完成，所有成功的开源项目都需要依靠社区群体的智慧，开源项目不应该与政治行为挂钩。

本分支移除了存在于 Notepad++ 内的作者个人政治内容，提供了完整的开发环境及开发流程，并遵循 GPL License 的要求完整开源。

## 📦 安装

提供 Windows 操作系统适配，支持 [Win32](https://downloadserver.soraharu.com:7000/Notepad++/8.3.2/npp.8.3.2.Installer.exe)、[x64](https://downloadserver.soraharu.com:7000/Notepad++/8.3.2/npp.8.3.2.Installer.x64.exe)、[ARM64](https://downloadserver.soraharu.com:7000/Notepad++/8.3.2/npp.8.3.2.Installer.arm64.exe) 架构。

访问本项目的 [Release](https://gitlab.soraharu.com/XiaoXi/Notepad-Plus-Plus/-/releases) 页面查看所有安装包。

## 🖥 编译环境

- Microsoft Visual Studio 2022 IDE ([Download](https://visualstudio.microsoft.com/zh-hans/vs/))
  - Desktop development with C++
    - C++ core desktop features
    - C++ CMake tools for Windows
    - Windows 10 SDK (10.0.19041.0)
    - MSVC v143 - VS 2022 C++ x64/86 build tools
    - MSVC v143 - VS 2022 C++ ARM64 build tools
    - C++ ATL for v143 build tools (x86 & x64)
    - C++ ATL for v143 build tools (ARM64)
    - Git for Windows

- NSIS 3.08 ([Download](https://nsis.sourceforge.io/Download))

- 7-Zip 21.07 ([Download](https://www.7-zip.org/))

## ⚙ 编译方式

1. 克隆本项目文件到本地
```shell
git clone https://gitlab.soraharu.com/XiaoXi/Notepad-Plus-Plus.git
```
2. 使用环境正确的 `Microsoft Visual Studio 2022 IDE` 打开 `PowerEditor\visual.net\notepadPlus.sln`
3. 编译并分别生成 `Release` 状态的 `Win32`、`x64`、`ARM64` 版本
4. 编辑 `PowerEditor\installer\packageAll.bat` 文件内的 `D:\Software\7-Zip\7z.exe` 为你的 7-Zip 软件位置
5. 执行 `packageAll.bat`
6. 如果一切正常的话，你将会在 `PowerEditor\installer\build` 中找到生成的所有软件包
